#!/bin/bash

# START ENVIRONMENT VARIABLES

# Define file path
HOME_PATH="~/.zshrc"
FILE_PATH=$(eval echo "$HOME_PATH")

# Create list
# https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_telemetry
# https://docs.brew.sh/Manpage
# Stop showing hostname and FQDN in Terminal
declare -a strings=(
'export HOMEBREW_NO_ANALYTICS="1"'
'export HOMEBREW_NO_INSECURE_REDIRECT="1"'
'export HOMEBREW_UPGRADE_GREEDY="1"'
'export POWERSHELL_TELEMETRY_OPTOUT="1"'
'export PS1="%1~ %# "'
)

# Check .zshrc else add
for line in "${strings[@]}"; do
    # Check if the file contains the line
    if ! grep -qF "$line" "$FILE_PATH"; then
      echo "$line" >> "$FILE_PATH"
    fi
done

# END ENVIRONMENT VARIABLES

# START PMSET
# hibernatemode 3 sleep / 25 hibernate
pmset -a hibernatemode 25
pmset -a womp 0
# END PMSET

# START FSTAB

# check existance of fstab
fstabchk="/etc/fstab"
if [ -e "$fstabchk" ]; then
    # echo 'File exists'
else
    # File does not exist create
    touch /etc/fstab
fi

# VisionTek VT4950 deny mount due to notification spamming "drive not ejected properly"

labelchk=$(grep -E "^(LABEL=MacKMLink none cd9660 ro,noauto)$" /etc/fstab)
if [ -n "$labelchk" ]; then
    # echo 'found all set'
else
    # echo 'Not found append to end of file'
    echo "LABEL=MacKMLink none cd9660 ro,noauto" | tee -a /etc/fstab
fi

exdisk=$(diskutil list physical | grep "0:" | grep -v "partition_scheme" | awk '{print $4}')
diskuuidchk=$(diskutil info $exdisk | grep "Volume UUID")
fschk=$(diskutil info $exdisk | grep "MS-DOS FAT12")
if [ "$diskuuidchk" != "" ] && [ "$fschk" != "" ]; then
    # Found a UUID and " File System Personality:   MS-DOS FAT12" proceed with checks
    uuid=$diskuuidchk | awk '{print $3}'
    uuidentry="UUID="
    uuidentry+="$uuid"
    uuidentry+=" none msdos ro,noauto"
    uuidentrychk=$(grep -E "^($uuidentry)$" /etc/fstab)
    if ["$uuidentrychk" = "" ]; then
        # Did not find UUID entry like below add it:
        # UUID=XXXXXXXX-XXXX-XXXX-XXXXX-XXXXXXXXXXXX none msdos ro,noauto
        echo $uuidentrychk | tee -a /etc/fstab
    fi
fi

# END VisionTek VT4950

# END FSTAB

# START DEFAULTS

autohide=$(defaults read com.apple.dock autohide-delay)
if [[ $autohide != "9999999" ]]; then
    # echo 'not 9999999 or "The domain/default pair of (com.apple.dock, autohide-delay) does not exist"'
    defaults write com.apple.dock autohide-delay 9999999
    killall Dock
fi

hiddenfiles=$(defaults read com.apple.finder AppleShowAllFiles)
if [[ $hiddenfiles != "true" ]]; then
    # echo 'not true or error "The domain/default pair of (com.apple.finder, AppleShowAllFiles) does not exist"'
    defaults write com.apple.finder AppleShowAllFiles true
    killall Finder
fi

# END DEFAULTS

# START HOMEBREW

hck=$(brew -v)
test="Homebrew"
if [[ $hck != *$test* ]]; then
    # echo 'homebrew not installed. Install it as per https://brew.sh/'
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
else
    # homebrew installed. Install apps
    brew install aria2
    brew install axel
    brew install --cask brave-browser
    brew install --cask expressvpn
    brew install --cask firefox
    brew install --cask flameshot
    # brew install --cask geany
    arch -arm64 brew install jq
    brew install --cask keepassxc
    brew install --cask krita
    brew install --cask libreoffice
    brew install mas
    brew install --cask microsoft-remote-desktop
    brew install --cask obs
    brew install ocrmypdf
    brew install --cask openshot-video-editor
    brew install --cask paintbrush
    brew install parallel
    brew install --cask pdfsam-basic
    brew install --cask powershell
    brew install --cask rectangle
    brew install rust
    brew install --cask utm
    brew install vscodium
    # brew install --cask visual-studio-code
    brew install --cask vivaldi
    brew install --cask vlc
fi

# END HOMEBREW

# START MAS

# UTC Time https://sindresorhus.com/utc-time mas not uninstalling or installing apps successfully anymore... Apple (accidentally?!) bricked it?
mas install 1538245904
# Xcode
mas install 497799835

# END MAS