#!/bin/bash
# Google Chrome Install as root
# OLD x86 version https://dl.google.com/chrome/mac/stable/GGRO/googlechrome.dmg > Version 87.0.4280.88 (Official Build) (x86_64 translated)
error=$(curl -s -S https://dl.google.com/chrome/mac/universal/stable/GGRO/googlechrome.dmg -o /tmp/googlechrome.dmg 2>&1 >/dev/null)
if [ "$error" != "" ]; then
    # echo "Download successful proceed installing"
    pkill -f "Google Chrome"
    rm -rf "Applications/Google Chrome.app"
    mount="/Volumes/GCTEMP"
    hdiutil attach -mountpoint $mount -nobrowse -noverify -quiet /tmp/googlechrome.tmp
    # cp command causes prompt: "Google Chrome" is damaged...
    # cp -r "$mount/Google Chrome.app" /Applications
    rsync -a "$mount/Google Chrome.app" /Applications
    hdiutil detach "$mount" -quiet
    rm -f /tmp/googlechrome.dmg
fi