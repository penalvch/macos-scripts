#!/bin/bash
# macOS checks tested on 14.2 M1
# CPU %
cpuidle=$(top -l 1 | grep "CPU usage" | awk '{print $7}')
cpuidle="${cpuidle%?}"
cpuidle=$(echo "scale=2;100-$cpuidle" | bc)
cpuhigh=90
result=$(echo "$cpuidle > $cpuhigh" | bc)
if["$result" -eq 1 ]; then
	# action
fi